<?php get_header(); ?>

<?php

$args = array(
    'post_type' => 'page',
    'post_status' => 'publish',
    'name' => 'experiential-technology'
);

$query = new WP_Query($args);

if ($query->have_posts()):

    while ($query->have_posts()):

        $query->the_post();

        // Variables

        $title = get_the_title();

        $content = get_the_content();

        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

        ?>


        <header id="top">

            <section class="page-intro" style="background: url('<?php echo $image[0]; ?>'); ">

                <section class="title-content-wrap flex">

                    <section class="flex">

                        <section>

                            <section class="pulse-button-container">

                                <span class="pulse-button"><span></span></span>

                            </section>
                            <!--/.pulse-button-container-->

                        </section>

                    </section>

                </section>
                <!--/.title-content-->

                <section class="inner-content-wrap">

                    <section class="inner">

                        <?php echo wpautop($content); ?>

                    </section>
                    <!--/.inner-->

                </section>
                <!--/.inner-content-wrap-->

            </section>
            <!--/.page-intro-->

        </header>
        <!--/#top-->

        <section id="mixed-reality" class="gray-to-white-bg">

        <div class="top-ellipses gray"></div>
        <!--/.top-ellipses-->

        <section class="ui container">

        <?php

        if (have_rows('mixed_reality')):

            while (have_rows('mixed_reality')) : the_row();

                ?>


                <?php

                if (get_row_layout() == 'content'):

                    // Variables

                    $mixed_reality_title = get_sub_field('mixed_reality_title');

                    $mixed_reality_content = get_sub_field('mixed_reality_content');

                    $virtual_reality_content = get_sub_field('virtual_reality_content');

                    $virtual_reality_image = get_sub_field('virtual_reality_image');

                    $augmented_reality_content = get_sub_field('augmented_reality_content');

                    $augmented_reality_image = get_sub_field('augmented_reality_image');

                    $how_can_mixed_reality_be_used_title = get_sub_field('how_can_mixed_reality_be_used_title');

                    $why_use_mixed_reality_title = get_sub_field('why_use_mixed_reality_title');

                    $engage_icon = get_sub_field('engage_icon');

                    $explore_icon = get_sub_field('explore_icon');

                    $gamify_icon = get_sub_field('gamify_icon');

                    $track_icon = get_sub_field('track_icon');

                    $platforms_devices_title = get_sub_field('platforms__+_devices_title');

                    $platforms_devices_image = get_sub_field('platforms_+_devices_image');

                    $end_to_end_support_title = get_sub_field('end_to_end_support_title');

                    ?>

                    <h1 class="center-align-text uppercase"><?php echo $mixed_reality_title; ?></h1>

                    <section class="inner center-align-text">

                        <?php echo $mixed_reality_content; ?>

                    </section>
                    <!--/.inner-->

                    <section class="icon-wrap clear-floats">

                        <section class="inner">

                            <img src="<?php echo $virtual_reality_image; ?>" alt="" class="icon">

                            <?php echo wpautop($virtual_reality_content); ?>

                        </section>
                        <!--/.inner-->

                        <section class="inner">

                            <img src="<?php echo $augmented_reality_image; ?>" alt="" class="icon">

                            <?php echo wpautop($augmented_reality_content); ?>

                        </section>
                        <!--/.inner-->

                    </section>
                    <!--/.icon-wrap-->

                    <h1 class="center-align-text uppercase"><?php echo $how_can_mixed_reality_be_used_title; ?></h1>

                    <?php

                    if (have_rows('mixed_reality_bullet_list')):

                        ?>

                        <section class="ui four column stackable grid" id="mixed-reality-uses">

                            <?php

                            while (have_rows('mixed_reality_bullet_list')) : the_row();

                                $bullet_list = get_sub_field('bullet_list_content');

                                ?>
                                <section class="column">
                                    <?php echo $bullet_list; ?>
                                </section>

                                <?php

                            endwhile;
                            ?>

                        </section>
                        <!--/.grid-->

                        <?php

                    endif;
                    ?>


                    <?php

                endif;

                ?>


                </section>
                <!--/.container-->

                <div class="bottom-ellipses gray"></div>
                <!--/.bottom-ellipses-->

                </section>
                <!--/#mixed-reality-->

                <section id="why-use-mixed-reality">

                    <section class="ui two column stackable centered grid">

                        <section class="column">

                            <section class="inner" id="explore-icon">

                                <section>
                                    <img src="<?php echo $explore_icon; ?>" alt="" class="icon">
                                </section>

                            </section>
                            <!--/.inner-->

                        </section>
                        <!--/.column-->

                        <section class="four column centered row">

                            <section class="column">

                                <section class="inner clear-floats" id="engage-icon">

                                    <img src="<?php echo $engage_icon; ?>" alt="" class="icon">

                                </section>
                                <!--/.inner-->

                            </section>
                            <!--/.column-->

                            <section class="column">

                                <section class="inner">

                                    <?php echo $why_use_mixed_reality_title; ?>

                                </section>
                                <!--/.inner-->

                            </section>
                            <!--/.column-->

                            <section class="column">

                                <section class="inner clear-floats" id="gamify-icon">

                                    <section>
                                        <img src="<?php echo $gamify_icon; ?>" alt="" class="icon">
                                    </section>

                                </section>
                                <!--/.inner-->

                            </section>
                            <!--/.column-->

                        </section>
                        <!--/.row-->

                        <section class="column">

                            <section class="inner" id="track-icon">

                                <section>

                                    <img src="<?php echo $track_icon; ?>" alt="" class="icon">

                                </section>

                            </section>
                            <!--/.inner-->

                        </section>
                        <!--/.column-->


                    </section>
                    <!--/.grid-->

                </section>
                <!--/#why-use-mixed-reality-->

                <section id="platforms" class="gray-to-white-bg">

                    <div class="top-ellipses gray"></div>
                    <!--/.top-ellipses-->

                    <section class="ui container">

                        <h1 class="uppercase"><?php echo $platforms_devices_title; ?></h1>

                    </section>
                    <!--/.container-->

                    <img src="<?php echo $platforms_devices_image; ?>" alt="">

                    <section class="line-container">
                        <div class="bottom-vertical-line"></div>
                    </section>
                    <!--/.line-container-->

                    <div class="bottom-ellipses gray"></div>
                    <!--/.bottom-ellipses-->

                </section>
                <!--/#platforms-->

                <section id="end-to-end-support">

                    <section class="ui container">

                        <h1 class="uppercase center-align-text"><?php echo $end_to_end_support_title; ?></h1>

                        <?php

                        if (have_rows('end_to_end_support')):

                            ?>

                            <section class="ui four column stackable grid">

                                <?php

                                while (have_rows('end_to_end_support')) : the_row();

                                    $image = get_sub_field('image');

                                    $content = get_sub_field('content');

                                    ?>
                                    <section class="column">

                                        <section class="inner">
                                            <img src="<?php echo $image; ?>" alt="">
                                            <?php echo $content; ?>
                                        </section>
                                        <!--/.inner-->

                                    </section>
                                    <!--/.column-->

                                    <?php

                                endwhile;
                                ?>

                            </section>
                            <!--/.grid-->

                            <?php

                        endif;
                        ?>

                    </section>
                    <!--/.container-->

                </section>
                <!--/#end-to-end-support-->

                <?php

            endwhile;
        endif;

    endwhile;
endif;

?>

<?php get_template_part('contact'); ?>

<?php get_footer(); ?>
