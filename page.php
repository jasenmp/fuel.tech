<?php
get_header();

if (have_posts()):

    while (have_posts()):

        the_post();

        // Variables

        $project_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

        ?>
        
        <section class="ui container" id="generic-page-container">

            <img src="<?php echo $project_image[0]; ?>" alt="">
            
        </section>


        <?php
    endwhile;
endif;
?>

<?php get_template_part('contact'); ?>

<?php
    get_footer();
?>
