<?php get_header(); ?>

<section class="ui container" id="generic-page-container">

    <p class="center-align-text">It looks like nothing was found at this location. Would you like to return to the <a href="<?php echo home_url(); ?>">home page?</a></p>

</section>

<?php get_template_part('contact'); ?>

<?php get_footer(); ?>
