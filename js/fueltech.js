jQuery(document).ready(function () {

    // AJAX Loading Animation

    $(document).ajaxStart(function () {
        $('.loader').show().velocity({rotateZ: "3600deg"}, 6000, "swing");
    });

    $(document).ajaxStop(function () {
        $('.loader').velocity('reverse').hide();
    });

    // Portfolio AJAX Calls


    // On Document Ready Call

    if ($('body.home,body.page-our-work').length) {

        var post_count = 6;

        $.ajax({

            cache: false,

            timeout: 8000,

            type: 'POST',

            url: ajax_object.ajaxurl,

            data: (
            {
                'action': 'portfolio',
                category_slug: 'all',
                post_count: post_count
            }

            ),

            beforeSend: function () {


            },

            success: function (data) {

                $('#portfolio .ajax-container').html(data);

            },

            error: function (errorThrown) {

                console.log(errorThrown);

            }

        });

    }

    // On Document Ready Call - Interior Pages

    if ($('.page-template-interior-page #portfolio').length) {

        // Portfolio Category Slug Names = pulled from interior-page.php

        var category_slug = jQueryArray;

        var post_count = 3;

        console.log(category_slug);

        $.ajax({

            cache: false,

            timeout: 8000,

            type: 'POST',

            url: ajax_object.ajaxurl,

            data: (
            {
                'action': 'portfolio',
                category_slug: category_slug,
                post_count: post_count
            }

            ),

            beforeSend: function () {


            },

            success: function (data) {

                $('#portfolio .ajax-container').html(data);

            },

            error: function (errorThrown) {

                console.log(errorThrown);

            }

        });

    }

    // Add CSS Class on Scroll - Interior Pages Only

    $('#wrapper').scroll(function () {

        var scroll = $('#wrapper').scrollTop();

        if (scroll >= 20) {

            $('#header-logo-container').addClass('scrolled');

            $('.home #header-logo-container img').css('display', 'block');

        } else {
            $('#header-logo-container').removeClass('scrolled');
            $('.home #header-logo-container img').css('display', 'none');

        }

    });


    // Dynamically Set Height of Wrapper - I can't even remember why I did this. May not be necessary.

    $('#wrapper').height($(window).height());

    $(window).resize(function () {
        $('#wrapper').height($(window).height());
    });

    $(window).trigger('resize');

    // Nice Scroll

    $('#wrapper,.fueltech-nav').niceScroll({
        cursorcolor: '#e57500',
        cursorborder: 'none',
        //horizrailenabled: false
    });

    // Rotating Banner Owl Carousel

    var rotatingBannerNavigation = $('.fueltech-dots')
        , rotaingBannerCarousel = $('#rotating-banner')
        , slideNo,
        prevSlideNo,
        owl_dots_html;


    $('#rotating-banner').owlCarousel({


        items: 1,

        onInitialized: function () {

            // Play video once owl carousel is initialized

            $('#rotating-banner .owl-item.active').find('video').each(function () {
                this.play();
            });

            $('#rotating-banner .owl-item.active .fueltech-dots .owl-dot:first-of-type').addClass('active-dot');

        },
        onTranslate: function () {

            // On banner change, pause the video

            $('#rotating-banner .owl-item').find('video').each(function () {
                this.pause();
            });

            // Capture previous slide No. - this is for custom pagination

            prevSlideNo = $('.owl-item.active .ui.grid').attr('id');

            // Remove active banner designation

            $('.owl-dot#' + prevSlideNo + '.active-dot').removeClass('active-dot');

        },
        onTranslated: function () {


            // Find videos and play them (if applicable)

            $('#rotating-banner .owl-item.active').find('video').each(function () {
                this.play();
            });

            // Capture slide No. for custom pagination

            slideNo = $('#rotating-banner .owl-item.active .ui.grid').attr('id')

            console.log($('.owl-dot#' + slideNo));

            // Apply active dot class to current pagination dot

            $('#rotating-banner .owl-item.active .ui.grid .owl-dot#' + slideNo).addClass('active-dot');

        },

        //autoplay: true,
        autoplayTimeout: 10000,
        loop: true,
        autoplayHoverPause: true,

    });

    // Select a single banner

    rotatingBannerNavigation.on('click', '.owl-dot', function () {

        // Remove active dot class

        slideNo = $(this).attr('id');

        rotaingBannerCarousel.trigger('to.owl.carousel', [slideNo, 500]);

    });


    //Menu

    var fueltech_nav = $('.fueltech-nav'),

        fueltech_logo = $('.logo-block img'),

        fueltech_interior_logo = $('.page #header-logo-container img'),

        fueltech_nav_wrapper_one = $('.fueltech-nav-wrapper:nth-child(1)'),

        fueltech_nav_wrapper_two = $('.fueltech-nav-wrapper:nth-child(2)'),

        fueltech_nav_wrapper_three = $('.fueltech-nav-wrapper:nth-child(3)'),

        fueltech_nav_wrapper_four = $('.fueltech-nav-wrapper:nth-child(4)'),

        fueltech_nav_wrapper_five = $('.fueltech-nav-wrapper:nth-child(5)'),

        fueltech_nav_title = $('.fueltech-navigation-title'),

        fueltech_nav_links = $('.fueltech-navigation-list dd a'),

        triggerNav = $('.cd-nav-trigger')


    triggerNav.funcToggle('click', function () {

        $(fueltech_nav).css('visibility', 'visible');

        $(this).addClass('nav-open');

        // Velocity Animation


        if (fueltech_interior_logo.length) {

            fueltech_interior_logo.velocity({

                properties: 'transition.fadeOut'

            }, {

                duration: 50

            });

        }

        var MenuSequence = [

            {
                e: fueltech_nav_wrapper_one
                , p: {
                height: '100%'
            }
                , o: {
                duration: 50
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
            }
            }, {
                e: fueltech_nav_wrapper_two
                , p: {
                    height: '100%'
                }
                , o: {
                    duration: 100
                    , easing: 'easeInOutQuart'
                    , sequenceQueue: false
                }
            }, {
                e: fueltech_nav_wrapper_three
                , p: {
                    height: '100%'
                }
                , o: {
                    duration: 150
                    , easing: 'easeInOutQuart'
                    , sequenceQueue: false
                }
            }, {
                e: fueltech_nav_wrapper_four
                , p: {
                    height: '100%'
                }
                , o: {
                    duration: 200
                    , easing: 'easeInOutQuart'
                    , sequenceQueue: false
                }
            }, {
                e: fueltech_nav_wrapper_five
                , p: {
                    height: '100%'
                }
                , o: {
                    duration: 250
                    , easing: 'easeInOutQuart'
                    , sequenceQueue: false
                }
            }, {
                e: fueltech_nav_title
                , p: 'transition.slideRightIn'
                , o: {
                    duration: 1000
                    , easing: 'easeInOutQuart'
                    , sequenceQueue: false
                }
            },

            {
                e: fueltech_logo
                , p: 'transition.slideRightIn'
                , o: {
                duration: 1100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
            }
            },

            {
                e: fueltech_nav_links
                , p: 'transition.fadeIn'
                , o: {
                duration: 1200,
                sequenceQueue: false
            }
            }

        ];

        $.Velocity.RunSequence(MenuSequence);

    }, function () {

        $(this).removeClass('nav-open');

        $('.velocity-animating').velocity('stop', true);

        if (fueltech_interior_logo.length) {

            fueltech_interior_logo.velocity({

                properties: 'transition.fadeIn'

            }, {

                duration: 900000

            });

        }

        // Velocity Animation

        var MenuSequence = [

            {
                e: fueltech_nav_wrapper_one
                , p: {
                height: '0'
            }
                , o: {
                duration: 100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
            }
            },

            {
                e: fueltech_nav_wrapper_two
                , p: {
                height: '0'
            }
                , o: {
                duration: 100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
            }
            },

            {
                e: fueltech_nav_wrapper_three
                , p: {
                height: '0'
            }
                , o: {
                duration: 100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
            }
            }, {
                e: fueltech_nav_wrapper_four
                , p: {
                    height: '0'
                }
                , o: {
                    duration: 100
                    , easing: 'easeInOutQuart'
                    , sequenceQueue: false
                }
            },

            {
                e: fueltech_nav_wrapper_five
                , p: {
                height: '0'
            }
                , o: {
                duration: 100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
            }
            },
            {
                e: fueltech_nav_links
                , p: 'transition.fadeOut'
                , o: {
                duration: 100,
                sequenceQueue: false
            }
            },
            {
                e: fueltech_nav_title
                , p: 'transition.slideRightOut'
                , o: {
                duration: 100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
                , complete: function () {
                    $(fueltech_nav).css('visibility', 'hidden');
                }
            }
            },
            {
                e: fueltech_logo
                , p: 'transition.slideRightOut'
                , o: {
                duration: 100
                , easing: 'easeInOutQuart'
                , sequenceQueue: false
                , complete: function () {
                    $(fueltech_nav).css('visibility', 'hidden');
                }
            }
            }

        ];

        $.Velocity.RunSequence(MenuSequence);

    });

    // Project Content

    var projectsContainer = $('#portfolio')
        , singleProjectContent = $('.cd-project-content')
        , projectID;


    // Select a single project - open project-content panel

    if (!Modernizr.touch) {

        projectsContainer.on('click', '.column a', function (event) {

            event.preventDefault();

            // Store project ID

            projectID = $(this).attr('id');

            // Dynamically Load Project


            $.ajax({

                cache: false,

                timeout: 8000,

                type: 'POST',

                url: ajax_object.ajaxurl,

                data: (
                {
                    'action': 'portfolio_project',
                    project_ID: projectID
                }

                ),

                beforeSend: function () {


                },

                success: function (data) {

                    if ($('#header-logo-container').length) {

                        $('#header-logo-container').velocity(
                            'transition.fadeOut', {

                                duration: 200
                            }
                        );

                    }

                    $('.cd-project-content').velocity(
                        'transition.slideLeftBigIn', {

                            duration: 1000,
                            complete: function () {
                                // Show Project Content Scrollbar

                                $('.cd-project-content').niceScroll({
                                    cursorcolor: '#e57500',
                                    cursorborder: 'none',
                                    horizrailenabled: false
                                });

                            }
                        }
                    );

                    $('.cd-project-content .ajax-container').html(data).velocity(
                        'transition.fadeIn', {

                            duration: 1000

                        }
                    );

                    // Init Video JS (If there is a video present)

                    if ($('.video-js-responsive-container').length) {

                        videojs(document.getElementsByClassName('video-js')[0], {

                            'controls': true,
                            'autoplay': false,
                            'preload': 'none',
                            'loop': true


                        }, function () {


                        });

                    }

                    // Init Owl Carousel

                    var projectCarousel = $('.cd-project-content .cd-project-content-carousel');

                    projectCarousel.owlCarousel({
                        items: 1,
                        nav: true,
                        navText: [
                            '<img src="' + my_data.template_directory_uri + '/images/icon_left_arrow.svg" alt="" width="30" />',
                            '<img src="' + my_data.template_directory_uri + '/images/icon_right_arrow.svg" alt="" width="30" />',
                        ],
                        onInitialized: function(){

                            $('.owl-prev').css('display','none')

                        },
                        onTranslate: function () {
                            $('.cd-project-content .owl-item').find('video').each(function () {
                                this.pause();
                            });
                        },
                        onTranslated: function () {

                            $('.cd-project-content .owl-item.active').find('video').each(function () {
                                this.play();
                            });

                            // Hide arrow if it's the last owl-item

                            if((projectCarousel).find('.owl-item').last().hasClass('active')){

                                $('.owl-next').css('display','none')

                            }else {
                                $('.owl-next').css('display','block')
                            }

                            if((projectCarousel).find('.owl-item').first().hasClass('active')){

                                $('.owl-prev').css('display','none')

                            }else {
                                $('.owl-prev').css('display','block')
                            }
                        }

                    });

                    // Reset Owl Carousel to first slide

                    projectCarousel.trigger('to.owl.carousel', [0, 500]);
                },

                error: function (errorThrown) {

                    console.log(errorThrown);

                }

            });

            $('.cd-nav-trigger').css('z-index', '1');

            //Hide scrollbar

            $('#wrapper').addClass('remove-scrollbar').getNiceScroll().remove();

        });

    } else {


        // Touch Device Event

        projectsContainer.on('click', '.column a', function (event) {

            if (!$(this).hasClass('hover')) {

                $('a.portfolio-container.hover .portfolio-description, a.portfolio-container.hover .portfolio-container-inner').velocity(
                    'finish'
                );

                $('a.portfolio-container.hover .portfolio-description, a.portfolio-container.hover .portfolio-container-inner').velocity(
                    'reverse'
                );

                $('#portfolio .hover').removeClass('hover');


                event.preventDefault();

                $(this).addClass('hover');

                $('.portfolio-container.hover .portfolio-container-inner').velocity(
                    'transition.whirlIn', {

                        duration: 500,
                        easing: 'easeInOutQuart',
                        delay: 100,
                        drag: true

                    }
                );

                $('a.portfolio-container.hover .portfolio-container-inner .portfolio-description').velocity(
                    'transition.slideDownIn',
                    {
                        delay: 500,
                        easing: 'easeInOutQuart'
                    }
                );

            } else {

                // Store project ID

                projectID = $(this).attr('id');

                // Dynamically Load Project


                $.ajax({

                    cache: false,

                    timeout: 8000,

                    type: 'POST',

                    url: ajax_object.ajaxurl,

                    data: (
                    {
                        'action': 'portfolio_project',
                        project_ID: projectID
                    }

                    ),

                    beforeSend: function () {


                    },

                    success: function (data) {

                        if ($('#header-logo-container').length) {

                            $('#header-logo-container').velocity(
                                'transition.fadeOut', {

                                    duration: 200
                                }
                            );

                        }

                        $('.cd-project-content').velocity(
                            'transition.slideLeftBigIn', {

                                duration: 1000,
                                complete: function () {
                                    // Show Project Content Scrollbar

                                    $('.cd-project-content').niceScroll({
                                        cursorcolor: '#e57500',
                                        cursorborder: 'none',
                                        horizrailenabled: false
                                    });

                                }
                            }
                        );

                        $('.cd-project-content .ajax-container').html(data).velocity(
                            'transition.fadeIn', {

                                duration: 1000

                            }
                        );

                        // Init Video JS (If there is a video present)

                        if ($('.video-js-responsive-container').length) {

                            videojs(document.getElementsByClassName('video-js')[0], {

                                'controls': true,
                                'autoplay': false,
                                'preload': 'none',
                                'loop': true


                            }, function () {


                            });

                        }

                        // Init Owl Carousel

                        var projectCarousel = $('.cd-project-content .cd-project-content-carousel');

                        projectCarousel.owlCarousel({
                            items: 1,
                            nav: true,
                            loop: true,
                            navText: [
                                '<img src="' + my_data.template_directory_uri + '/images/icon_left_arrow.svg" alt="" width="30" />',
                                '<img src="' + my_data.template_directory_uri + '/images/icon_right_arrow.svg" alt="" width="30" />',
                            ],
                            onTranslate: function () {
                                $('.cd-project-content .owl-item').find('video').each(function () {
                                    this.pause();
                                });
                            },
                            onTranslated: function () {
                                $('.cd-project-content .owl-item.active').find('video').each(function () {
                                    this.play();
                                });
                            }

                        });

                        // Reset Owl Carousel to first slide

                        projectCarousel.trigger('to.owl.carousel', [0, 500]);
                    },

                    error: function (errorThrown) {

                        console.log(errorThrown);

                    }

                });

                $('.cd-nav-trigger').css('z-index', '1');

                //Hide scrollbar

                $('#wrapper').addClass('remove-scrollbar').getNiceScroll().remove();

            }

        });

    }

    // Close single project content


    if (!Modernizr.touch) {

        singleProjectContent.on('click', '.close', function (event) {

            event.preventDefault();

            // Pause Video Video

            $('.cd-project-content').find('video').each(function () {
                this.pause();
            });

            singleProjectContent.velocity(
                'transition.slideRightBigOut', {

                    duration: 1000,
                    complete: function () {

                        if ($('#header-logo-container').length) {

                            $('#header-logo-container').velocity(
                                'transition.fadeIn', {

                                    duration: 500
                                }
                            );

                        }
                    }

                }
            );

            $('.cd-nav-trigger').css('z-index', '20');

            // Show scrollbar

            $('#wrapper').removeClass('remove-scrollbar').niceScroll({
                cursorcolor: '#e57500',
                cursorborder: 'none',
                horizrailenabled: false
            });


        });

    } else {

        // Touch Device Event - Remove Hover from Project Link

        singleProjectContent.on('click', '.close', function (event) {

            event.preventDefault();

            // Pause Video Video

            $('.cd-project-content').find('video').each(function () {
                this.pause();
            });

            singleProjectContent.velocity(
                'transition.slideRightBigOut', {

                    duration: 1000,
                    complete: function () {

                        if ($('#header-logo-container').length) {

                            $('#header-logo-container').velocity(
                                'transition.fadeIn', {

                                    duration: 500
                                }
                            );

                        }
                    }

                }
            );

            $('.cd-nav-trigger').css('z-index', '20');

            // Show scrollbar

            $('#wrapper').removeClass('remove-scrollbar').niceScroll({
                cursorcolor: '#e57500',
                cursorborder: 'none',
                horizrailenabled: false
            });

            // Remove Hover From Project Link

            $('a.portfolio-container.hover .portfolio-description, a.portfolio-container.hover .portfolio-container-inner').velocity(
                'finish'
            );

            $('a.portfolio-container.hover .portfolio-description, a.portfolio-container.hover .portfolio-container-inner').velocity(
                'reverse'
            );

            $('#portfolio .hover').removeClass('hover');

        });
    }


    // Portfolio Filter

    var portfolioFilter = $('#portfolio-filter');

    // Select Filter  - Select a single project - open project-content panel

    $(function () {

        $(this).change(function () {

            // Get Category Slug

            var category_slug = $('#selectFilter').val();

            // Post Count

            var post_count = 6

            $.ajax({

                cache: false,

                timeout: 8000,

                type: 'POST',

                url: ajax_object.ajaxurl,

                data: (
                {
                    'action': 'portfolio',
                    category_slug: category_slug,
                    post_count: post_count
                }

                ),

                beforeSend: function () {


                },

                success: function (data) {

                    $('#portfolio .ajax-container').html(data);

                },

                error: function (errorThrown) {

                    console.log(errorThrown);

                }

            });

        });

    });

    // Text Filter - Select a single project category

    portfolioFilter.on('click', 'li a', function (event) {

        event.preventDefault();

        $('#portfolio-filter a.active-filter').removeClass('active-filter');


        // Finish and reverse the previous Ajax Container Animation

        $('#portfolio .ajax-container.velocity-animating').velocity(
            'finish'
        );

        $('#portfolio .ajax-container.velocity-animating').velocity(
            'reverse'
        );


        $(this).addClass('active-filter');

        // Get Category Slug

        var category_slug = $(this).attr('data-category-slug');

        // Post Count

        var post_count = 6

        $.ajax({

            cache: false,

            timeout: 8000,

            type: 'POST',

            url: ajax_object.ajaxurl,

            data: (
            {
                'action': 'portfolio',
                category_slug: category_slug,
                post_count: post_count
            }

            ),

            beforeSend: function () {


            },

            success: function (data) {

                $('#portfolio .ajax-container').html(data).velocity(
                    'transition.slideLeftBigIn', {

                        duration: 1000
                    }
                );

            },

            error: function (errorThrown) {

                console.log(errorThrown);

            }

        });

    });

    // Portfolio Project Rollovers

    if (!Modernizr.touch) {

        $('#portfolio').on('mouseenter', 'a', function () {

            $(this).addClass('hover');


            $('a.portfolio-container.hover .portfolio-container-inner').velocity(
                'transition.whirlIn', {

                    duration: 500,
                    easing: 'easeInOutQuart',
                    delay: 100,
                    drag: true

                }
            );

            $('a.portfolio-container.hover .portfolio-container-inner .portfolio-description').velocity(
                'transition.slideDownIn',
                {
                    delay: 500,
                    easing: 'easeInOutQuart'
                }
            );

        });

        $('#portfolio').on('mouseleave', 'a', function () {

            $('a.portfolio-container.hover .portfolio-description, a.portfolio-container.hover .portfolio-container-inner').velocity(
                'finish'
            );

            $('a.portfolio-container.hover .portfolio-description, a.portfolio-container.hover .portfolio-container-inner').velocity(
                'reverse'
            );

            $(this).removeClass('hover');

        });

    }

    // We Innovate

    // Initial Document Ready AJAX Call

    if ($('body.home').length && $('#software-applications').length) {

        $.ajax({

            cache: false,

            timeout: 8000,

            type: 'POST',

            url: ajax_object.ajaxurl,

            data: (
            {
                'action': 'we_innovate',
                software_ID: 'Realty4D'
            }

            ),

            success: function (data) {

                $('#software-applications .ajax-container').html(data);

                $('#software-applications').attr('data-current-id', 'Realty4D');

                $('#logo-navigation').on('mouseenter', 'img', we_innovate_mouseenter);

            },

            error: function (errorThrown) {

                console.log(errorThrown);

            }

        });

    }


    // We Innovate MouseEnter Event

    function we_innovate_mouseenter() {

        // Store filter ID

        var softwareFilterID = $(this).attr('data-type-id');

        // Add active logo class

        $(this).addClass('active-logo');

        // If the selected section isn't already active...

        if (softwareFilterID !== $('#software-applications').attr('data-current-id')) {

            // Remove active logo class

            $('#logo-navigation img').removeClass('active-logo');

            $.ajax({

                cache: false,

                timeout: 8000,

                type: 'POST',

                url: ajax_object.ajaxurl,

                data: (
                {
                    'action': 'we_innovate',
                    software_ID: softwareFilterID
                }

                ),

                beforeSend: function () {


                },

                success: function (data) {

                    $('#software-applications .ajax-container').html(data).velocity(
                        'transition.slideLeftIn', {

                            duration: 800

                        }
                    );

                    setTimeout(function () {

                        $('#software-applications .ajax-container').find('video').each(function () {
                            this.play();
                        });

                    }, 2000);

                    $('#software-applications').attr('data-current-id', softwareFilterID);

                    $('#logo-navigation').on('mouseenter', 'img', we_innovate_mouseenter);

                },

                error: function (errorThrown) {

                    console.log(errorThrown);

                }

            });

        }

    };

    // We Innovate Waypoint

    if ($('#software-applications').length) {

        var softwareWaypoint = new Waypoint({
            element: document.getElementById('software-applications'),
            handler: function (direction) {

                if (direction === 'down') {

                    $('#software-applications').find('video').each(function () {
                        this.play();
                    });
                }
            },
            context: document.getElementById('wrapper'),
            offset: 400
        })


    }


    // Client Filter

    var clientFilter = $('#clients-filter')
        , clientFilterID;

    // Select Filter  Select a client category

    $(function () {

        $('#clientSelectFilter').change(function () {

            clientFilterID = $(this).val();

            // Hide previous

            $('.client-list-container').removeClass('active-clients');

            // Display current

            $('.client-list-container#' + clientFilterID).addClass('active-clients');


        });

    });

    // Text Filter Select a client category

    clientFilter.on('click', 'li a', function (event) {

        event.preventDefault();

        // Store filter ID

        clientFilterID = $(this).attr('id');

        // Remove active filter class

        $('#clients-filter li a.active-filter').removeClass('active-filter');

        // Add active filter class

        $(this).addClass('active-filter');

        // Hide previous

        $('.client-list-container').removeClass('active-clients');

        // Display current

        $('.client-list-container#' + clientFilterID).addClass('active-clients');

        $('.client-list-container#' + clientFilterID).velocity(
            'transition.slideUpIn',
            {

                duration: 1000

            }
        );


    });

    // CTA

    var cta = $('#cta .ui.grid')
        , ctaID;


    // CTA Mouseenter

    cta.on('mouseenter', '.column', function () {

        $(this).addClass('hover');

        $(this).find('.default-content').velocity(
            'transition.fadeOut',
            {
                duration: 300,
                delay: 100,
                easing: 'easeInOutQuart',
            }
        );

        $(this).find('.hover-bg').velocity(
            'transition.fadeIn',
            {
                duration: 300,
                delay: 300,
                easing: 'easeInOutQuart',
            }
        );

        $(this).find('.secondary-content').velocity(
            'transition.fadeIn',
            {
                duration: 300,
                delay: 400,
                easing: 'easeInOutQuart',
            }
        );


    });

    // CTA Mouseleave

    cta.on('mouseleave', '.column', function () {

        $(this).find('.hover-bg, .secondary-content').velocity(
            'stop'
        );

        $(this).find('.hover-bg, .secondary-content').velocity(
            'reverse',
            {
                duration: 300,
                easing: 'easeInOutQuart',
            }
        );


        $(this).find('.default-content').velocity(
            'transition.fadeIn',
            {
                duration: 300,
                delay: 300,
                easing: 'easeInOutQuart',
            }
        );


    });

    // Coin Flip

    $('.icon-container .inner').flip({
        axis: 'y',
        trigger: 'hover'
    });

});