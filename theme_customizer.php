<?php

// ------------------------------------------------------------------
// Theme Customizer
// ------------------------------------------------------------------
//


class theme_Customizer
{

    public static function register($wp_customize)
    {

        // ------------------------------------------------------------------
        // Home Page
        // ------------------------------------------------------------------
        //


        $wp_customize->add_section(

            'home_page',

            array(

                'title' => __('Home Page', 'home_page_sections'),

                'priority' => 30,

            ));

        // ------------------------------------------------------------------
        // We Fuel Section Setting - Title
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'we_fuel_section',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'we_fuel_section',

                array(

                    'label' => __('Enter We Fuel Title', 'customizer_we_fuel_title_label'),

                    'section' => 'home_page',

                    'settings' => 'we_fuel_section',

                    'type' => 'text',
                )
            ));

        // ------------------------------------------------------------------
        // Fuel for Thought Section Setting - Title
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'fuel_for_thought_section',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'fuel_for_thought_section',

                array(

                    'label' => __('Enter Fuel for Thought Title', 'customizer_fuel_for_thought_title_label'),

                    'section' => 'home_page',

                    'settings' => 'fuel_for_thought_section',

                    'type' => 'text',
                )
            ));

        // ------------------------------------------------------------------
        // Fuel for Thought Section Setting - Copy
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'fuel_for_thought_copy_section',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'fuel_for_thought_copy_section',

                array(

                    'label' => __('Enter Fuel for Thought Copy', 'customizer_fuel_for_thought_copy_label'),

                    'section' => 'home_page',

                    'settings' => 'fuel_for_thought_copy_section',

                    'type' => 'textarea',
                )
            ));

        // ------------------------------------------------------------------
        // Social Media Links
        // ------------------------------------------------------------------
        //


        $wp_customize->add_section(

            'social_links',

            array(

                'title' => __('Social Media Links', 'social_links_sections'),

                'priority' => 30,

            ));

        // ------------------------------------------------------------------
        // Facebook Setting
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'facebook_link',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'facebook_link',

                array(

                    'label' => __('Enter Facebook URL', 'customizer_facebook_label'),

                    'section' => 'social_links',

                    'settings' => 'facebook_link',

                    'type' => 'text',
                )
            ));

        // ------------------------------------------------------------------
        // Youtube Setting
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'youtube_link',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'youtube_link',

                array(

                    'label' => __('Enter YouTube URL', 'customizer_youtube_label'),

                    'section' => 'social_links',

                    'settings' => 'youtube_link',

                    'type' => 'text',
                )
            ));

        // ------------------------------------------------------------------
        // Linkedin Setting
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'linkedin_link',

            array(

                'default' => ''
            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'linkedin_link',

                array(

                    'label' => __('Enter Linkedin URL', 'customizer_linkedin_label'),

                    'section' => 'social_links',

                    'settings' => 'linkedin_link',

                    'type' => 'text',
                )
            ));


        // ------------------------------------------------------------------
        // Twitter Setting
        // ------------------------------------------------------------------
        //

        $wp_customize->add_setting(

            'twitter_link',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'twitter_link',

                array(

                    'label' => __('Enter Twitter URL', 'customizer_twitter_label'),

                    'section' => 'social_links',

                    'settings' => 'twitter_link',

                    'type' => 'text',
                )
            ));

        // ------------------------------------------------------------------
        // Logo Upload
        // ------------------------------------------------------------------
        //

        $wp_customize->add_section(

            'company_logo_upload_section',

            array(

                'title' => __('Company Logo', 'company_logo_section'),

                'priority' => 30,

            ));

        $wp_customize->add_setting(

            'company_logo',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(
            new WP_Customize_Upload_Control(

                $wp_customize,

                'company_logo',

                array(

                    'label' => __('Company Logo', 'company_logo_label'),

                    'section' => 'company_logo_upload_section',

                    'settings' => 'company_logo',
                ))
        );

        // ------------------------------------------------------------------
        // Copyright
        // ------------------------------------------------------------------
        //

        $wp_customize->add_section(

            'copyright_options_section',

            array(

                'title' => __('Copyright Options', 'copyright_options_section'),

                'priority' => 30,

            ));

        $wp_customize->add_setting(

            'copyright_options',

            array(

                'default' => ''

            ));

        $wp_customize->add_control(

            new WP_Customize_Control(

                $wp_customize,

                'copyright_options',

                array(

                    'label' => __('Copyright Text', 'copyright_text_label'),

                    'section' => 'copyright_options_section',

                    'settings' => 'copyright_options',

                    'type' => 'text',
                )
            ));

    }

}

// Setup the Theme Customizer settings and controls...

add_action('customize_register', array('theme_Customizer', 'register'));