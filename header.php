<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta charset="<?php bloginfo('charset'); ?>"/>

    <meta name="author" content="Name">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <script src="https://use.typekit.net/ejh8yuf.js"></script>

    <script>
        try {
            Typekit.load({
                async: true
            });
        } catch (e) {
        }
    </script>

    <title><?php wp_title('', false) ?><?php bloginfo('name'); ?> <?php if(!is_home()){ echo '&ndash; '.get_the_title(); } ?></title>

    <?php wp_head(); ?>

    <?php include('analyticstracking.php'); ?>

</head>

<?php


?>

<body <?php body_class(); ?> id="top">

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<?php get_template_part('navigation'); ?>

<section id="wrapper" class="<?php if (!is_home()): echo 'interior-page'; endif; ?>">

    
    <?php get_template_part('interior-page-header'); ?>

