<section id="we-innovate">

    <section class="line-container">
        <div class="bottom-vertical-line"></div>
        <!--/.bottom-vertical-line-->
    </section>
    <!--/.line-container-->


    <?php

    $args = array(
        'post_type' => 'home_page',
        'post_status' => 'publish',
        'name' => 'we-innovate'
    );

    $query = new WP_Query($args);

    if ($query->have_posts()):

    while ($query->have_posts()):

    $query->the_post();

    // Variables

    $title = get_the_title();

    $content = get_the_content();

    ?>

    <h4 class="orange uppercase center-align-text section-title"><?php echo $title; ?></h4>

    <section class="ui container" class="intro">

        <section class="inner">

            <?php echo wpautop($content); ?>

        </section>
        <!--/.inner-->


    </section>
    <!--./container-->

    <section id="software-applications" class="hide-on-mobile">

        <section class="loader-container">
            <img class="loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/loader-gray2.png" alt="">
        </section>

        <section class="ajax-container">


        </section>
        <!--/.ajax-container-->

    </section>
    <!--/.#software-applications-->

    <section id="software-applications" class="show-on-mobile">

    <?php


    if (have_rows('video')):

    while (have_rows('video')) :
    the_row();

    // Variables

    $video_src = get_sub_field('video_file');

    $video_poster_src = get_sub_field('poster');

    $logo_src = get_sub_field('logo');

    $content = get_sub_field('description');

    ?>

        <section class="ui two column stackable grid">

            <section class="column" id="software-application-video" data-type-id="<?php echo $software_ID; ?>">

                <section class="responsive-video-wrapper">

                    <video loop muted preload="none" poster="<?php echo $video_poster_src; ?>" class="hide-on-mobile">
                        <source src="<?php echo $video_src; ?>" type="video/mp4"/>
                    </video>

                    <img src="<?php echo $video_poster_src; ?>" alt="" class="ui fluid image show-on-mobile">

                </section>
                <!--/.responsive-video-wrapper -->

            </section>
            <!--/#software-application-video-->
            <section class="column" id="software-application-copy">

                <section class="flex">

                    <section class="inner">

                        <img src="<?php echo $logo_src; ?>" alt="">

                        <?php echo wpautop($content); ?>


                    </section>
                    <!--/.inner-->

                </section>
                <!--/.flex-->

            </section>
            <!--/#software-application-copy-->


        </section>
        <!--/.grid-->


        <?php
            endwhile;
        endif;
        ?>

    </section>
    <!--/#software-applications.show-on-mobile-->


</section>
<!--/#we-innovate-->

<?php
endwhile;
endif;
?>