<section id="contact">

    <section class="inner">

        <?php

        if (!is_home()):

            ?>

            <h3 class="uppercase">Thanks for Discovering</h3>

            <?php

        else:
            ?>

            <h3>Let's</h3>

            <?php

        endif;
        ?>

        <img class="logo" src="images/logo_fueltech_no_tagline.svg">

        <p>Learn more about our experiential technology and what it
            <br/> can do for you by contacting us today!</p>

    </section>
    <!--/.inner-->

    <section class="ui container">

        <section class="ui three column stackable grid">

            <section class="column">

                <h5 class="uppercase">Get in Touch</h5>
                <p>855.472.7316</p>

            </section>
            <!--/.column-->

            <section class="column">

                <h5 class="uppercase">Visit Us</h5>
                <p>1811 Bering Drive</p>
                <p>Suite 450</p>
                <p>Houston, TX 77057</p>

            </section>
            <!--/.column-->

            <section class="column">

                <h5 class="uppercase">Email Us</h5>
                <a href="mailto:info@fuelfx.com?subject=Fuel.Tech Website Inquiry">info@fuel.tech</a>

            </section>
            <!--/.column-->

        </section>
        <!--/.grid-->

    </section>
    <!--/.container-->

</section>
<!--/#contact-->