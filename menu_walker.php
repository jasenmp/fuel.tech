<?php

/**
 * Created by PhpStorm.
 * User: jasen
 * Date: 6/20/2016
 * Time: 2:53 PM
 */

class FuelTech_Nav_Walker extends Walker
{

    static $count = 0;

    public $test;

    var $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');

    function start_lvl(&$output, $depth, $args = array())
    {


        if($this->test > 6){
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<dl class='fueltech-navigation-list float-left'>\n";
        }else {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<dl class='fueltech-navigation-list'>\n";
        }

    }

    function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent</dl>\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        global $wp_query;

        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $locations = get_nav_menu_locations();

        $menu = wp_get_nav_menu_object( $locations['primary-menu'] );

        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $count = 0;

        foreach( $menu_items as $menu_item ){
            if( $menu_item->menu_item_parent == $item->ID ){
                $this->test = $count++;
            }
        }

        $classes = empty($item->classes) ? array() : (array)$item->classes;

        /* Add active class */
        if (in_array('current-menu-item', $classes)) {
            $classes[] = 'active';
            unset($classes['current-menu-item']);
        }

        /* Check for children */
        $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
        if (!empty($children)) {
            $classes[] = 'has-sub';
        }else {
            $classes[] = 'clear';
        }

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        // Count Menu Items

        global $menu_items;

        $menu_items = substr_count($output,'<dd');

        // Close dl and open new dl.fueltech-navigation-list after 6th menu item to separate into columns

        if ($menu_items == 6) {
            $output .= '</dl><dl class="fueltech-navigation-list float-left" id="'.$count.'">';
        }

        // Navigation Logo (found in functions.php)

        global $nav_logo;

        // Home URL

        global $home_url;

        // If has children and is the first menu item - Insert the Logo, If has children then wrap in section > section.inner else open dd

        if( !empty($children) && $item->title == 'We Are <br/> Creative + Technologist') {
            $output .= $indent . '<section' . $id . $value . $class_names . '><section class="logo-block"><a href="'.$home_url.'"><img src="'.$nav_logo.'"/></a></section><section class="inner" id="'.$count.'">';
        }
        elseif (!empty($children)) {
            $output .= $indent . '<section' . $id . $value . $class_names . '><section class="inner" id="'.$count.'">';
        }
        else
        {
            $output .= $indent . '<dd' . $value . $class_names . 'id="'.$menu->count.'">';
        }



        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

        $item_output = $args->before;

        if ($depth==0) {

            $item_output .= '<dt class="fueltech-navigation-title" id="'.$count.'"><span>';

        } else {
            $item_output .= '<a' . $attributes . '>';
        }



        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;

        if ($depth==0) {

            $item_output .= '</span></dt>';

        } else {
            $item_output .= '</a>';
        }


        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);

        self::$count++;  // increase counter

    }

    function end_el(&$output, $item, $depth, $args = array())
    {

        $locations = get_nav_menu_locations();

        $menu = wp_get_nav_menu_object( $locations['primary-menu'] );

        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $count = 0;

        foreach( $menu_items as $menu_item ){
            if( $menu_item->menu_item_parent == $item->ID ){
                $this->test = $count++;
            }
        }

        $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));


        if (!empty($children)) {
            if (!empty($children) && $count <= 6)
            {
                $output .= "</section></section>\n";
            }
            elseif(!empty($children) && $count >= 12)
            {
                $output .= "</dl></section></section>\n";
            }
            else
            {
                $output .= "</dd>\n";
            }


        }
    }

}


