<?php

global $post;

$args = array(
    'post_type' => 'page',
    'post_status' => 'publish',
    'pagename' => 'contact'
);

$query = new WP_Query($args);

if ($query->have_posts()):

    while ($query->have_posts()):

        $query->the_post();

        if( !is_home() ) {

            $content = get_field('interior_page_contact');

        }else {
            $content = get_the_content();
        }



        ?>

        <section id="contact">

            <!--/.inner-->

            <section class="ui container">

                <section class="ui two column stackable grid">

                    <?php

                    if (have_rows('contact_info')):

                        while (have_rows('contact_info')): the_row();

                            $column_one = get_sub_field('column_one');
                            $column_two = get_sub_field('column_two');
                            $column_three = get_sub_field('column_three');
                            $column_four = get_sub_field('column_four');

                            ?>

                            <section class="column">

                                <section class="inner">

                                    <?php echo wpautop($content); ?>

                                </section>

                                <section class="ui two column stackable grid">

                                    <section class="column">
                                        <?php echo $column_one; ?>
                                    </section>

                                    <section class="column">
                                        <?php echo $column_three; ?>
                                    </section>

                                </section>
                                <!--/.grid-->

                                <h5 class="uppercase center-align-text" style="margin-top: 45px;">Visit Us</h5>

                                <section class="ui two column stackable grid">

                                    <section class="column">
                                        <?php echo $column_two; ?>
                                    </section>

                                    <section class="column">
                                        <?php echo $column_four; ?>
                                    </section>

                                </section>

                            </section>
                            <!--/.column-->

                            <section class="column">

                                <?php echo do_shortcode('[contact-form-7 id="602" title="Primary Contact Form"]'); ?>

                            </section>
                            <!--/.column-->


                            <?php

                        endwhile;
                    endif;

                    ?>

                </section>
                <!--/.grid-->

            </section>
            <!--/.container-->


        </section>
        <!--/#contact-->

        <?php
    endwhile;
endif;
?>