<section id="creative-technologists">


    <?php

    // INTRO SECTION

    $args = array(
        'post_type' => 'home_page',
        'post_status' => 'publish',
        'name' => 'creative-technologist'
    );

    $query = new WP_Query($args);

    if ($query->have_posts()):

        while ($query->have_posts()):

            $query->the_post();

            // Variables

            $content = get_the_content();

            $mobile_content = get_field('mobile_creative_technology_copy');

            ?>

            <section class="inner hide-on-mobile">

                <section class="ui container">
                    <section class="title">

                        <?php echo wpautop($content); ?>

                    </section>
                </section>
                <!--/.container-->

            </section>
            <!--/.inner#hide-on-mobile-->

            <section id="creative-technologist-mobile" class="show-on-mobile">

                <?php echo wpautop($mobile_content); ?>

                <section class="ui right aligned two column grid">

                    <?php

                    if (have_rows('coins')):

                        $loop = 1;

                        while (have_rows('coins')): the_row();

                            if (have_rows('coin')):

                                // Open .column & .circle-background.left & .ui.grid.inner

                                echo '<section class="column">';

                                while (have_rows('coin')): the_row();

                                    // Close .column & Open new .column

                                    if ($loop % 7 === 0) {

                                        echo '</section><section class="column">';

                                    }

                                    // Variables

                                    $front = get_sub_field('front');

                                    $back = get_sub_field('back');

                                    $page_link = get_sub_field('page_link');

                                    // Coins


                                    // Open .icon-container

                                    echo '<section class="icon-container">';

                                    // Open .inner

                                    echo '<section class="inner">';

                                    // Open .front

                                    echo '<section class="front">';

                                    echo '<img src="' . $front . '" class="icon">';

                                    // End .front

                                    echo '</section>';

                                    // Open .back

                                    echo '<section class="back flex">';

                                    echo '<section class="copy">';

                                    echo $back;

                                    echo '<a class="coin-page-link" href="'.$page_link.'">Learn More</a>';

                                    // Close .copy

                                    echo '</section>';

                                    // Close .back

                                    echo '</section>';

                                    // Close .inner

                                    echo '</section>';

                                    // .icon-container

                                    echo '</section>';

                                    $loop++;

                                endwhile;

                                // Close sections

                                echo '</section>';

                            endif;

                        endwhile;

                    endif;

                    ?>

                </section>
                <!--/.grid-->

            </section>
            <!--/.creative-technologist-mobile#show-on-mobile-->

            <section id="creative-technologist-grid" class="hide-on-mobile">

                <div class="battery flex">

                    <section class="inner">
                        <?php

                        $battery_image = get_field('battery_image');

                        echo '<img src="' . $battery_image . '"/>';

                        ?>
                    </section>
                    <!--/.inner-->

                </div>
                <!--/.battery-->

                <section class="ui two column grid containing">


                    <?php

                    if (have_rows('coins')):

                        $loop = 1;

                        while (have_rows('coins')): the_row();

                            if (have_rows('coin')):

                                // Open .column & .circle-background.left & .ui.grid.inner

                                echo '<section class="column"><section class="circle-background left"><section class="ui grid inner">';

                                while (have_rows('coin')): the_row();

                                    // Close .column & .circle-background.left & .ui.grid.inner & .row Open .column & .circle-background.right & .ui.grid.inner

                                    if ($loop % 7 === 0) {

                                        echo '</section></section></section><section class="column"><section class="circle-background right"><section class="ui grid inner">';

                                    }

                                    // Variables

                                    $front = get_sub_field('front');

                                    $back = get_sub_field('back');

                                    $page_link = get_sub_field('page_link');

                                    // Coins

                                    // Coins - Open .row & .column

                                    if ($loop === 1 || $loop === 3 || $loop === 5 || $loop === 7 || $loop === 9 || $loop === 11) {

                                        echo '<section class="row"><section class="column">';

                                    }

                                    // Open .icon-container

                                    echo '<section class="icon-container">';

                                    // Open .inner

                                    echo '<section class="inner">';

                                    // Open .front

                                    echo '<section class="front">';

                                    echo '<img src="' . $front . '" class="icon">';

                                    // End .front

                                    echo '</section>';

                                    // Open .back

                                    echo '<section class="back flex">';

                                    echo '<section class="copy">';

                                    echo $back;

                                    echo '<a class="coin-page-link" href="'.$page_link.'">Learn More</a>';

                                    // Close .copy

                                    echo '</section>';

                                    // Close .back

                                    echo '</section>';

                                    // Close .inner

                                    echo '</section>';

                                    // .icon-container

                                    echo '</section>';

                                    // Coins - Close .column & .row

                                    if ($loop === 2 || $loop == 4 || $loop == 6 || $loop == 8 || $loop == 10 || $loop == 12) {

                                        echo '</section></section>';
                                    }

                                    $loop++;

                                endwhile;

                                // Close sections

                                echo '</section></section>';

                            endif;

                        endwhile;

                    endif;

                    ?>

                </section>
                <!--/.ui.two.column.grid-->

            </section>
            <!--/#creative-technologist-grid-->

            <?php

        endwhile;

    endif;


    ?>

</section>
</section>
<!--/#creative technologists-->