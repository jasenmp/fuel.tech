<?php

// AJAX Calls for Portfolio


add_action('wp_ajax_portfolio', 'prefix_ajax_portfolio');

add_action('wp_ajax_nopriv_portfolio', 'prefix_ajax_portfolio');


function prefix_ajax_portfolio()
{

    // Variables

    $args = array(
        'post_type' => 'portfolio',
        'post_status' => 'publish',
        'posts_per_page' => $_POST['post_count'],
        'tax_query' => array(

            array(
                'taxonomy' => 'portfolio_type',
                'field' => 'slug',
                'terms' => $_POST['category_slug']
            ),
        )
    );

    $portfolio_query = new WP_Query($args);

    $loop = 1;

    echo '<section class="row">';

    while ($portfolio_query->have_posts()) : $portfolio_query->the_post();

        // Variables

        $project_ID = get_the_ID();

        $project_image = wp_get_attachment_image_src(get_post_thumbnail_id($project_ID), 'full-size');

        $project_logo_image = get_field('company_logo');

        $project_title = get_the_title();

        if($loop % 4 == 0){

            echo '</section><section class="row">';

        }


        ?>

        <section class="column">

            <a href="#" class="portfolio-container" id="<?php echo $project_ID; ?>">

                <img src="<?php echo $project_image[0]; ?>" alt=""
                     class="ui fluid image">

                <section class="portfolio-container-inner">

                    <section class="portfolio-description">

                        <section class="flex">

                            <section class="inner">

                                <section class="project-logo">
                                    <img src="<?php echo $project_logo_image; ?>" alt="">
                                </section>
                                <!--/.project-logo-->

                                <h3><?php echo $project_title; ?></h3>

                            </section>

                        </section>

                    </section>
                    <!--/.portfolio-description-->

                </section>
                <!--/.inner-->

            </a>
            <!--/.portfolio-container-->

        </section>
        <!--/.container-->

        <?php

        $loop++;

    endwhile;

    echo '</section>';

    // Always die in functions echoing ajax content

    exit();

}