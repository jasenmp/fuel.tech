<?php
/**
 * Template Name: Interior Page
 * User: jasen
 * Date: 6/17/2016
 * Time: 6:03 PM
 */


get_header();


if (have_posts()):

    while (have_posts()):

        the_post();


        // Get The Page Slug for sections with unique layouts

        $slug = get_post_field('post_name');

        // Get Portfolio Category Slug for Work Section

        $categories = get_the_terms($post->ID, 'portfolio_category_slug');

        $cat_array = [];

        foreach ($categories as $category) {

            $category_list = $category->slug;

            $cat_array[] = $category->slug;

        }

        ?>

        <script type="text/javascript">
            // Store Portfolio Category Slugs, send to AJAX
            var jQueryArray = <?php echo json_encode($cat_array); ?>
        </script>

        <?php

        if (have_rows('interior_page_options')):

            while (have_rows('interior_page_options')) :
                the_row();

                if (get_row_layout() == 'content'):

// Variables

                    $page_icon = get_sub_field('page_icon');

                    $page_title = get_sub_field('page_title');


                    ?>


                    <header>

                        <section class="page-intro">

                            <section class="icon">

                                <img src="<?php echo $page_icon; ?>" alt="">

                                <h4><?php the_title(); ?></h4>

                            </section>
                            <!--/.icon-->

                            <h1 class="title uppercase"><?php echo $page_title; ?></h1>

                            <?php the_post_thumbnail('full-size', array('class' => 'hero-image')); ?>

                            <section class="pulse-button-container">

                <span class="pulse-button"><span></span></span>

                            </section>
                            <!--/.pulse-button-container-->

                            <section class="inner">

                                <?php the_content(); ?>

                            </section>
                            <!--/.inner-->

                        </section>
                        <!--/.intro-->

                    </header>
                    <!--/header-->


                    <!-- Marketing Page Only -->

                    <?php

                    if ($slug == 'marketing'):

                        $content = get_field('end_to_end_campaigns');

                        ?>

                        <section class="ui container" id="marketing-process">

                            <?php

                            if (have_rows('marketing_process')):
                                ?>

                                <section class="ui four column stackable grid">

                                    <?php

                                    while (have_rows('marketing_process')) : the_row();

                                        $icon = get_sub_field('icon');
                                        $copy = get_sub_field('copy');

                                        ?>

                                        <section class="column">

                                            <section class="marketing-process-inner">
                                                <img src="<?php echo $icon; ?>" alt="">
                                                <?php echo $copy; ?>
                                            </section>
                                            <!--/.marketing-process-inner-->

                                        </section>
                                        <!--/.column-->

                                        <?php
                                    endwhile;
                                    ?>

                                </section>
                                <!--/.grid-->

                                <?php

                            endif;

                            ?>

                        </section>
                        <!--/#marketing-process-->

                        <section class="inner center-align-text" id="end-to-end-campaigns">

                            <?php echo $content; ?>

                        </section>
                        <!--/.inner-->

                        <?php

                    endif;

                    ?>
                    <!-- / Marketing Page Only -->

                    <section id="cta">


                        <?php

                        if (have_rows('cta')):

                        $count = 1;

                        ?>
                        <?php

                        while (have_rows('cta')) : the_row();

                            $columns = $count++;

                            ?>

                            <?php
                        endwhile;
                        ?>

                        <section class="
        <?php

                        if ($columns == 3 || $columns == 6):

                            echo 'ui three column stackable grid';

                        else: echo 'ui two column stackable grid';

                        endif;
                        ?>
    ">

                            <?php

                            while (have_rows('cta')) : the_row();

                                $columns = $count++;

                                $content = get_sub_field('content');

                                $inner_content = get_sub_field('inner_content');

                                $image = get_sub_field('image');

                                ?>


                                <section class="column">

                                    <section class="inner" style="background: url('<?php echo $image; ?>')">


                                        <section class="default-content content visible-content">


                                            <div class="tint"></div>
                                            <!--/.tint-->

                                            <section class="content-wrap flex">

                                                <?php echo $content; ?>

                                            </section>
                                            <!--/.content-wrap-->

                                        </section>
                                        <!--/.default-content-->

                                        <section class="secondary-content content hidden-content">

                                            <div class="hover-bg"></div>
                                            <!--/.hover-bg-->

                                            <div class="tint"></div>
                                            <!--/.tint-->

                                            <section class="content-wrap flex">

                                                <?php echo $inner_content; ?>

                                            </section>
                                            <!--/.content-wrap-->

                                        </section>
                                        <!--/.secondary-content-->

                                    </section>
                                    <!--/.inner-->

                                </section>
                                <!--/.column-->
                                <?php

                            endwhile;
                            ?>

                            <?php
                            endif;
                            ?>

                        </section>
                        <!--/.grid-->

                    </section>
                    <!--/#cta-->

                    <?php

                    if (have_rows('services')):

                        $count = 1;

                        ?>

                        <h1 class="center-align-text uppercase">Services</h1>

                        <section class="ui container">

                            <section id="services">

                                <?php


                                while (have_rows('services')) : the_row();

                                    $columns = $count++;

                                endwhile;

                                ?>

                                <section class="
                                    <?php

                                if ($columns == 2 || $columns == 3):

                                    echo 'ui three column stackable centered grid';

                                else:

                                    echo 'ui four column stackable centered grid';

                                endif;
                                ?>
                                ">

                                    <?php

                                    while (have_rows('services')) : the_row();

                                        $bullet_list = get_sub_field('bullet_list');

                                        ?>

                                        <section class="column">

                                            <?php echo $bullet_list; ?>

                                        </section>

                                        <?php

                                    endwhile;

                                    ?>
                                </section>
                                <!--/.grid-->


                            </section>
                            <!--/#services-->

                        </section>

                        <?php

                    endif;

                    ?>


                    <section id="portfolio">

                        <h1 class="center-align-text uppercase">Work</h1>

                        <section class="loader-container">
                            <img class="loader"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/images/loader-gray2.png" alt="">
                        </section>

                        <section class="ajax-container ui three column grid">

                        </section>
                        <!--/.grid-->

                    </section>
                    <!--/#portfolio-->

                    <section class="cd-project-content">

                        <section class="loader-container">
                            <img class="loader"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/images/loader-gray2.png" alt="">
                        </section>

                        <section class="ajax-container">

                        </section>
                        <!--/.ajax-container-->

                    </section>
                    <!--/.cd-project-content-->

                    <?php

                    // End Flexible Content loop

                endif;
            endwhile;
        endif;

        ?>

        <?php

        // End WP Loop

    endwhile;
endif;

?>

<?php get_template_part('contact'); ?>

<?php get_footer(); ?>

