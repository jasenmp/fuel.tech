<?php

// AJAX Calls for Portfolio


add_action('wp_ajax_portfolio_project', 'prefix_ajax_portfolio_project');

add_action('wp_ajax_nopriv_portfolio_project', 'prefix_ajax_portfolio_project');


function prefix_ajax_portfolio_project()
{

    // Variables

    $args = array(
        'p' => $_POST['project_ID'],
        'post_type' => 'portfolio',
        'post_status' => 'publish',
    );

    $portfolio_query = new WP_Query($args);

    while ($portfolio_query->have_posts()) : $portfolio_query->the_post();

        // Variables

        $project_ID = get_the_ID();

        $project_image = get_field('project_image');

        $project_image_background = wp_get_attachment_image_src(get_post_thumbnail_id($project_ID), 'full-size');

        $project_title = get_the_title();

        $company_logo = get_field('company_logo');

        $content = get_the_content('', FALSE, ''); // arguments remove 'more' text

        $delieverables = get_field('delieverables')

        ?>

        <section class="cd-project-container">

            <section class="cd-project-container-background"
                     style="background: url('<?php echo $project_image_background[0]; ?>') no-repeat;">

                <div class="cd-project-container-tint"></div>

                <section class="inner logo flex">

                    <section class="project-company-logo">
                        <img src="<?php echo $company_logo; ?>">
                    </section>
                    <!--/.project-company-logo-->

                </section>
                <!--/.inner-->

                <section class="inner intro">

                    <section class="inner">

                        <section class="project-content">

                            <section class="ui centered stackable grid">

                                <section class="eleven wide column">

                                    <h5>Overview</h5>

                                    <?php

                                    echo do_shortcode(wpautop($content));

                                    ?>
                                </section>
                                <!--/.column-->

                                <section class="three wide column project-sidebar">

                                    <section class="sidebar-block">

                                        <h5>Project</h5>

                                        <p><?php echo $project_title; ?></p>

                                    </section>

                                    <section class="sidebar-block">

                                        <h5>Delieverables</h5>

                                        <?php echo wpautop($delieverables); ?>

                                    </section>

                                </section>

                                <!--/.column-->

                            </section>
                            <!--/.grid-->

                        </section>
                        <!--/.project-content-->

                    </section>

                </section>


            </section>
            <!--/.cd-project-container-background-->

            <section class="cd-project-hero">

                <img src="<?php echo $project_image_background[0]; ?>" class="ui fluid image" alt="">

            </section>
            <!--/.cd-project-hero-->
            
            <section class="cd-project-carousel-container">
                
                <section class="cd-project-carousel-container-inner">

                    <?php

                    if (have_rows('carousel')):

                        echo '<section class="cd-project-content-carousel owl-carousel manual">';

                        while (have_rows('carousel')) : the_row();

                            $media = get_sub_field('media');

                            $media_url = wp_get_attachment_url($media['id']);

                            // Get file extension

                            $path_info = pathinfo(get_attached_file($media['id']));


                            ?>


                            <?php

                            if ($path_info['extension'] == 'svg' || $path_info['extension'] == 'jpg' || $path_info['extension'] == 'png' || $path_info['extension'] == 'gif'):

                                ?>

                                <section>

                                    <img src="<?php echo $media['url']; ?>" alt="" class="ui fluid image">

                                </section>
                                <!--/Images-->

                                <?php

                            endif;

                            if (($path_info['extension'] == 'mp4')):

                                ?>


                                <section class="video-js-responsive-container vjs-hd">

                                    <section class="video-js-responsive">

                                        <video class="video-js vjs-default-skin vjs-big-play-centered">
                                            <source src="<?php echo $media['url']; ?>" type="video/mp4"/>
                                        </video>

                                    </section>
                                    <!--/.responsive-video-wrapper -->

                                </section>
                                <!--/Video-->

                                <?php

                            endif;

                            ?>


                            <?php

                        endwhile;

                        // End .cd-project-content-carousel

                        echo '</section>';

                    endif;

                    ?>
                    
                </section>
                <!--/.cd-project-carousel-container-inner-->
                
            </section>
            <!--/.cd-project-carousel-container-->


            <section class="cd-project-hero">

                <img src="<?php echo $project_image_background[0]; ?>" class="ui fluid image" alt="">

            </section>
            <!--/.cd-project-hero-->


            <section class="cd-project-hero cd-project-hero-footer">
                
                <section class="cd-project-hero-footer-inner">

                    <img src="http://cdn-s.hellomonday.com/assets/images/projects/desktop/google-ideas/image-4.jpg" class="ui fluid image" alt="">
                    
                </section>

            </section>
            <!--/.cd-project-hero-->

        </section>
        <!--/.cd-project-container-->

        <a href="#" class="close cd-img-replace">Close</a>

        <?php

    endwhile;

    // Always die in functions echoing ajax content

    exit();

}