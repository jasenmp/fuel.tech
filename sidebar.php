<section class="five wide column">

    <section class="sidebar-block">

        <h5>Recent Post</h5>

        <ul>

            <?php

            $args = array(

                'numberposts' => 5,
                'orderby' => 'post_date',
                'post_type' => 'post',
                'post_status' => 'publish'

            );

            $recent_posts = wp_get_recent_posts($args);

            foreach ($recent_posts as $recent):

                ?>

                <li><a href="<?php echo get_permalink($recent['ID']); ?>"><?php echo $recent['post_title']; ?></a>
                    <section class="post-date"><?php echo mysql2date('M j Y', $recent['post_date']); ?></section>
                </li>

                <?php
            endforeach;
            ?>

        </ul>

    </section>
    <!--/sidebar--block-->

    <section class="sidebar-block">

        <h5>Categories</h5>

        <ul>

            <?php


            $category_args = array(
                'post_type' => 'post',
                'hide_empty' => 1,
                'exclude' => array(1),
            );

            $categories = get_categories($category_args);

            foreach ($categories as $category):

                $category_link = get_category_link($category->cat_ID);

                ?>

                <li><a href="<?php echo esc_url($category_link); ?>"><?php echo $category->name; ?></a></li>

                <?php

            endforeach;

            ?>
        </ul>

    </section>
    <!--/.sidebar-block-->


    <section class="sidebar-block">

        <h5>Archives</h5>

        <ul>

            <?php


                $archive_args = array(
                    'post_type' => 'post',
                    'type' => 'monthly',
                );

                wp_get_archives($archive_args);

            ?>


        </ul>


    </section>

</section>