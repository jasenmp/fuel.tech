
<?php

$company_logo = get_theme_mod('company_logo');

?>

<section id="header-logo-container">

    <section class="ui two column grid">

        <section class="column">
            <a href="<?php echo get_home_url(); ?>">
                <img src="<?php echo $company_logo; ?>" alt="" id="header-logo">
            </a>
        </section>

        <section class="column uppercase">

            <section class="breadcrumb float-right">
                <?php custom_breadcrumbs(); ?>
            </section>
            <!--/.breadcrumb-->

        </section>

    </section>

</section>