<section id="our-clients">

    <section class="line-container">
        <div class="bottom-vertical-line"></div>
        <!--/.bottom-vertical-line-->
    </section>
    <!--/.line-container-->


    <?php

    $args = array(
        'post_type' => 'home_page',
        'post_status' => 'publish',
        'name' => 'our-clients'
    );

    $query = new WP_Query($args);

    if ($query->have_posts()):

    while ($query->have_posts()):

    $query->the_post();

    // Variables

    $title = get_the_title();

    $content = get_the_content();

    ?>

    <h4 class="orange uppercase center-align-text section-title"><?php echo $title; ?></h4>

    <section class="center-align-text">
        <?php echo wpautop($content); ?>
    </section>

    <section class="ui container">

        <?php

        if (have_rows('clients')):

            while (have_rows('clients')) : the_row();

                // Variables

                $label = get_sub_field('label');

                echo '<p class="client-type-label">' . $label . '</p>';

                echo '<section class="ui four column stackable grid">';

                if (have_rows('logos')):

                    while (have_rows('logos')) : the_row();


                        // Variables

                        $image = get_sub_field('image');


                        echo '<section class="column"><section class="inner"><img src="'.$image['url'].'" alt="'.$image['alt'].'"></section></section>';


                    endwhile;

                endif;

                echo '</section>'; // close .grid

            endwhile;
        endif;

        ?>

    </section>
    <!--/.contaiiner-->


</section>
<!--/#our-clients-->

<?php

endwhile;
endif;

?>
