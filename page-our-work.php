<?php get_header(); ?>


<section id="we-fuel">

    <section class="title-container">

        <section id="portfolio-filter">


            <div class="ui container hide-on-mobile" id="link-filter">

                <ul>
                    <?php

                    $i = 0;

                    $category_args = array(

                        'taxonomy' => 'portfolio_type',
                        'post_type' => 'portfolio',
                        'exclude' => array(17),

                    );

                    $categories = get_categories($category_args);

                    foreach ($categories as $category) {

                        if ($i == 0) {
                            echo '<li><a class="active-filter" href="#" data-category-slug="' . $category->slug . '">' . $category->name . '</a></li>';
                        } else {
                            echo '<li><a href="#" data-category-slug="' . $category->slug . '">' . $category->name . '</a></li>';
                        }

                        $i++;

                    }

                    ?>
                </ul>

            </div>
            <!--/#link-filter.container-->

            <div class="ui container show-on-mobile" id="select-filter">


                <section class="inner">

                    <form action="" class="ui form">

                        <select name="" id="selectFilter" class="ui fluid dropdown">
                            <?php

                            $category_args = array(

                                'taxonomy' => 'portfolio_type',
                                'post_type' => 'portfolio',
                                'exclude' => array(17),

                            );

                            foreach ($categories as $category) {

                                echo '<option value="' . $category->slug . '">' . $category->name . '</option>';

                            }

                            ?>
                        </select>

                    </form>
                    <!--/form-->

                </section>
                <!--/.inner-->


            </div>
            <!--/#select-filter.container-->

        </section>
        <!--/#portfolio-filter-->

    </section>
    <!--/.title-container-->

    <section id="portfolio">

        <section class="loader-container">
            <img class="loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/loader-gray2.png" alt="">
        </section>

        <section class="ajax-container ui three column stackable grid">

        </section>
        <!--/.grid-->

    </section>
    <!--/#portfolio-->

    <section class="cd-project-content">

        <section class="loader-container">
            <img class="loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/loader-gray2.png" alt="">
        </section>

        <section class="ajax-container">

        </section>
        <!--/.ajax-container-->

    </section>
    <!--/.cd-project-content-->

</section>
<!--/#we-fuel-->


<?php get_footer(); ?>
