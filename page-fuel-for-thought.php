<?php get_header(); ?>

    <section class="blog-list">

        <section class="blog-header">

            <section class="ui container">

                <h1 class="uppercase"><?php echo single_post_title(); ?></h1>

            </section>
            <!--/.container-->

        </section>
        <!--/.blog-header-->

        <section class="ui container">

            <section class="ui stackable grid">

                <section class="eleven wide column">

                    <?php

                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

                    $args = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'paged' => $paged
                    );

                    $query = new WP_Query($args);

                    if ($query->have_posts()):

                        while ($query->have_posts()):

                            $query->the_post();

                            // Variables

                            $title = get_the_title();

                            $excerpt = get_the_excerpt();

                            $link = get_permalink();

                            $post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');


                            ?>

                            <article>


                                <a href="<?php echo $link; ?>"><img src="<?php echo $post_image[0]; ?>"
                                                                    class="ui fluid image"></a>

                                <h2><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h2>

                                <section class="post-date">
                                    <?php echo get_the_date( 'F j, Y' ); ?>
                                </section>
                                <!--/.post-date-->

                                <?php echo wpautop($excerpt); ?>

                                <a class="read-more" href="<?php echo $link; ?>">Read More</a>

                                <section class="post-categories">

                                    <?php echo get_the_category_list(' | '); ?>

                                </section>
                                <!--/.post-categories-->

                            </article>
                            <!--/article-->

                            <?php

                        endwhile;
                    endif;
                    ?>

                    <section class="pagination">

                        <?php

                            if (function_exists(custom_pagination)){

                                custom_pagination($query->max_num_pages, '', $paged);

                            }

                        ?>

                    </section>

                </section>
                <!--/.column-->

                <?php get_sidebar(); ?>
                <!--/Sidebar-->

            </section>
            <!--/.grid-->

        </section>
        <!--/.container-->

    </section>
    <!--/.blog-list-->


<?php get_footer(); ?>