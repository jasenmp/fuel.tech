<?php get_header(); ?>

    <header id="top">

        <?php get_template_part('rotating-banner'); ?>

        <section class="pulse-button-container">

            <span class="pulse-button"><span></span></span>

            <section class="line-container">
                <div class="bottom-vertical-line"></div>
                <!--/.bottom-vertical-line-->
            </section>
            <!--/.line-container-->

        </section>
        <!--/.pulse-button-container-->

    </header>
    <!--/header-->


<?php get_template_part('creative_technologist'); ?>

<?php get_template_part('we-fuel'); ?>

<?php get_template_part('we-innovate'); ?>

<?php get_template_part('we-incubate'); ?>

<?php get_template_part('our-clients'); ?>

<?php get_template_part('fuel-for-thought'); ?>

<?php get_template_part('contact'); ?>

<?php get_footer(); ?>