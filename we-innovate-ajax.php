<?php

// AJAX Calls for We Innovate


add_action('wp_ajax_we_innovate', 'prefix_ajax_we_innovate');

add_action('wp_ajax_nopriv_we_innovate', 'prefix_ajax_we_innovate');


function prefix_ajax_we_innovate()
{

    // Variables

    $software_ID = $_POST['software_ID'];

    global $wpdb;

    $rows = $wpdb->get_results($wpdb->prepare(
        "
            SELECT * 
            FROM {$wpdb->prefix}postmeta
            WHERE meta_key LIKE %s
                AND meta_value = %s
            ",
        'video_%_id', // meta_name: $ParentName_$RowNumber_$ChildName
        $software_ID // meta_value: 'type_3' for example
    ));

    if ($rows) {

        foreach ($rows as $row) {

            // for each result, find the 'repeater row number' and use it to load the sub field!

            preg_match('_([0-9]+)_', $row->meta_key, $matches);

            // Variables - $matches[0] contains the row number!

            $meta_video_key = 'video_' . $matches[0] . '_video_file';

            $meta_video_poster_key = 'video_' . $matches[0] . '_poster';

            $meta_logo_key = 'video_' . $matches[0] . '_logo';

            $meta_content_key = 'video_' . $matches[0] . '_description';

            $video_id = get_post_meta($row->post_id, $meta_video_key, true);

            $video_poster_id = get_post_meta($row->post_id, $meta_video_poster_key, true);

            $logo_id = get_post_meta($row->post_id, $meta_logo_key, true);

            $video_src = wp_get_attachment_url($video_id);

            $video_poster_src = wp_get_attachment_url($video_poster_id);

            $logo_src = wp_get_attachment_url($logo_id);

            $content = get_post_meta($row->post_id, $meta_content_key, true);

        }

        ?>

        <section class="ui two column stackable grid">

            <section class="column" id="software-application-video" data-type-id="<?php echo $software_ID; ?>">

                <section class="responsive-video-wrapper">

                    <video loop muted preload="none" poster="<?php echo $video_poster_src; ?>" class="hide-on-mobile">
                        <source src="<?php echo $video_src; ?>" type="video/mp4"/>
                    </video>

                    <img src="<?php echo $video_poster_src; ?>" alt="" class="ui fluid image show-on-mobile">

                </section>
                <!--/.responsive-video-wrapper -->

            </section>
            <!--/#software-application-video-->

            <section class="column" id="software-application-copy">

                <section class="flex">

                    <section class="inner">

                        <img src="<?php echo $logo_src; ?>" alt="">

                        <?php echo wpautop($content); ?>

                        <?php

                        $args = array(
                            'post_type' => 'home_page',
                            'post_status' => 'publish',
                            'name' => 'we-innovate'
                        );

                        $query = new WP_Query($args);

                        if ($query->have_posts()):

                            while ($query->have_posts()):

                                $query->the_post();

                                ?>

                                <section class="inner" id="logo-navigation">


                                    <?php

                                    if (have_rows('logo_navigation')):

                                        $loop = 1;

                                        while (have_rows('logo_navigation')) : the_row();

                                            $src = get_sub_field('logo');

                                            $id = get_sub_field('id');

                                            ?>

                                            <img src="<?php echo $src['url']; ?>" data-type-id="<?php echo $id; ?>" alt="">

                                            <?php

                                            $loop++;

                                        endwhile;

                                    endif;

                                    ?>
                                </section>
                                <!--/#logo-navigation-->

                                <?php

                            endwhile;
                        endif;
                        ?>

                    </section>
                    <!--/.inner-->

                </section>
                <!--/.flex-->

            </section>
            <!--/#software-application-copy-->

        </section>
        <!--/.grid-->

        <?php
    }

    // Always die in functions echoing ajax content

    exit();

}