<section id="fuel-for-thought">

    <section class="line-container">
        <div class="bottom-vertical-line"></div>
        <!--/.bottom-vertical-line-->
    </section>
    <!--/.line-container-->

    <?php

    // Variables

    $title = get_theme_mod('fuel_for_thought_section');

    $content = get_theme_mod('fuel_for_thought_copy_section');

    ?>

    <section class="ui container">


        <h4 class="orange uppercase center-align-text section-title"><?php echo $title; ?></h4>

        <p class="center-align-text"><?php echo $content; ?></p>


        <?php

        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 3
        );

        $query = new WP_Query($args);

        if ($query->have_posts()):

            while ($query->have_posts()):

                $query->the_post();

                $news_title = get_the_title();

                $news_excerpt = get_the_excerpt();

                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

                $link = get_permalink();

                ?>

                <section class="news-item">

                    <a href="<?php echo $link; ?>" style="background: url('<?php echo $image[0]; ?>') no-repeat center;">

                        <section class="inner">

                            <h2><?php echo $news_title; ?></h2>

                            <p><?php echo $news_excerpt; ?></p>

                        </section>
                        <!--/.inner-->

                    </a>

                </section>


                <?php
            endwhile;
        endif;
        ?>

    </section>
    <!--/.container-->

</section>
<!--/#fuel-for-thought-->
