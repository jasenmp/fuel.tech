<?php wp_footer(); ?>
<footer>
    <section class="pulse-button-container">

        <span class="pulse-button" onclick="$('#top').animatescroll({element: '#wrapper' });"><span></span></span>

    </section>
    <!--/.pulse-button-container-->

    <?php

    $copyright = get_theme_mod('copyright_options');

    ?>


    <p><?php echo '&copy;' . ' ' . date('Y') . ' ' . $copyright; ?></p>
</footer>
<!--/footer-->

</section>
<!--/#wrapper-->

<!-- Preloader -->
<script type="text/javascript">
    //<![CDATA[
    $(window).load(function () { // makes sure the whole site is loaded
        $('#status').fadeOut(); // will first fade out the loading animation
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(350).css({'overflow': 'visible'});
    })
    //]]>
</script>


<script type='text/javascript' src='<?php echo get_site_url();?>/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.40.0-2013.08.13'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"loaderUrl":"<?php echo get_site_url();?>/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo get_site_url();?>/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.5.2'></script>


</body>

</html>