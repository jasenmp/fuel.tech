<?php

get_header();


if (have_posts()):

    while (have_posts()):

        the_post();

        // Variables

        $title = get_the_title();

        $content = get_the_content();

        $link = get_permalink();

        $post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

        $source_title = get_field('source_title');

        $source = get_field('source');

        ?>

        <section class="blog-list single-blog">

            <section class="blog-header">
                
                <div class="tint"></div>

                <section class="ui container">

                    <h2><?php echo $title; ?></h2>

                    <section class="post-date">
                        <?php echo get_the_date( 'F j, Y' ); ?>
                    </section>
                    <!--/.post-date-->

                    <section class="post-categories">

                        <?php echo get_the_category_list(' | '); ?>

                    </section>
                    <!--/.post-categories-->

                </section>
                <!--/.container-->

            </section>
            <!--/.blog-header-->

            <section class="ui container">

                <section class="ui stackable grid">

                    <section class="eleven wide column">

                        <article>

                           <img src="<?php echo $post_image[0]; ?>" class="ui fluid image feat-image">

                            <?php echo wpautop($content); ?>

                            <?php if(get_field('source')): ?>

                            <section class="source">
                               <p>Source: <a href="<?php echo $source; ?>"><?php echo $source_title;?></a></p>
                            </section>
                            <!--/.source-->

                            <?php endif; ?>

                        </article>
                        <!--/article-->
                    </section>
                    <!--.column-->

                    <?php get_sidebar(); ?>
                    <!--/Sidebar-->

                </section>
                <!--/.grid-->

            </section>
            <!--/.container-->

        </section>
        <!--/.blog-list-->

        <?php

        // End WP Loop

    endwhile;
endif;

?>

<?php get_footer(); ?>

