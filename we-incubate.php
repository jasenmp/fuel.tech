<section id="we-incubate">

    <section class="line-container">
        <div class="bottom-vertical-line"></div>
        <!--/.bottom-vertical-line-->
    </section>
    <!--/.line-container-->


    <?php

    $args = array(
        'post_type' => 'home_page',
        'post_status' => 'publish',
        'name' => 'we-incubate'
    );

    $query = new WP_Query($args);

    if ($query->have_posts()):

    while ($query->have_posts()):

    $query->the_post();

    // Variables

    $title = get_the_title();

    $content = get_the_content();

    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

    ?>

    <section id="we-incubate-mobile" class="show-on-mobile">

        <section class="inner">

            <h4 class="orange uppercase center-align-text section-title"><?php echo $title; ?></h4>

            <?php echo wpautop($content); ?>

        </section>
        <!--/.inner-->

    </section>
    <!--/#we-incubate-mobile-->

    <img src="<?php echo $image[0]; ?>" alt="">

    <section class="line-container">
        <div class="bottom-vertical-line"></div>
        <!--/.bottom-vertical-line-->
    </section>
    <!--/.line-container-->

    <section class="circle flex hide-on-mobile">

        <section class="inner">

            <h4 class="orange uppercase center-align-text section-title"><?php echo $title; ?></h4>

            <?php echo wpautop($content); ?>

        </section>
        <!--/.inner-->

    </section>
    <!--/circle-->

</section>
<!--/#we-incubate-->

<?php

endwhile;
endif;

?>
