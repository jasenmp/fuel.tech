<button class="cd-nav-trigger">Menu<span aria-hidden="true" class="cd-icon"></span></button>
<!--/menu trigger-->

<?php
    wp_nav_menu(array(
        'menu' => 'Primary Menu',
        'walker' => new FuelTech_Nav_Walker(),
        'container' => 'nav',
        'container_class' => 'fueltech-nav',
        'items_wrap' => '%3$s',
    ));
?>
