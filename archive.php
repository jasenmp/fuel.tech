<?php

get_header();

?>

<section class="blog-list single-blog">

    <section class="blog-header">

        <div class="tint"></div>

        <section class="ui container">

            <h2>Archives &ndash; <?php echo the_archive_title(); ?></h2>

        </section>
        <!--/.container-->

    </section>
    <!--/.blog-header-->

    <section class="ui container">

        <section class="ui grid">

            <section class="eleven wide column">

                <?php

                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

                if (have_posts()):

                    while (have_posts()):

                        the_post();

                        // Variables

                        $title = get_the_title();

                        $excerpt = get_the_excerpt();

                        $link = get_permalink();

                        $post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

                        $source_title = get_field('source_title');

                        $source = get_field('source');


                        ?>

                        <article>


                            <a href="<?php echo $link; ?>"><img src="<?php echo $post_image[0]; ?>"
                                                                class="ui fluid image"></a>

                            <h2><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h2>

                            <section class="post-date">
                                <?php echo get_the_date('F j, Y'); ?>
                            </section>
                            <!--/.post-date-->

                            <?php echo wpautop($excerpt); ?>

                            <a class="read-more" href="<?php echo $link; ?>">Read More</a>

                            <section class="post-categories">

                                <?php echo get_the_category_list(' | '); ?>

                            </section>
                            <!--/.post-categories-->

                        </article>
                        <!--/article-->
                        <?php

                        // End WP Loop

                    endwhile;

                endif;

                ?>

                <section class="pagination">

                    <?php

                    if (function_exists(custom_pagination)){

                        custom_pagination($query->max_num_pages, '', $paged);

                    }

                    ?>

                </section>

            </section>
            <!--.column-->

            <?php get_sidebar(); ?>
            <!--/Sidebar-->

        </section>
        <!--/.grid-->

    </section>
    <!--/.container-->

</section>
<!--/.blog-list-->


<?php get_footer(); ?>

