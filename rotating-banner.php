<section id="rotating-banner" class="owl-carousel">

    <?php

    $args = array(
        'post_type' => 'banner',
        'post_status' => 'publish'
    );

    $i = 0;

    $query = new WP_Query($args);

    if ($query->have_posts()):

        while ($query->have_posts()):

            $query->the_post();

            // Variables

            $count = $query->post_count; // Get Post Count for Dot Navigation

            $title = get_the_title();

            $content = get_the_content();

            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full-size');

            $page_link = get_field('page_link');

            $video = get_field('video');

            $video_poster = get_field('video_poster');

            ?>

            <section class="ui middle aligned two column centered grid" id="<?php echo $i++; ?>">

                <div class="column">

                    <div class="ui text container">

                        <?php

                        $company_logo = get_theme_mod('company_logo');

                        ?>

                        <img class="logo" src="<?php echo $company_logo; ?>" alt="Fuel.Tech Logo">

                        <h1><?php echo $title; ?></h1>

                        <?php echo wpautop($content); ?>

                        <a href="<?php echo $page_link; ?>">Learn More</a>

                        <section class="fueltech-dots">

                            <?php

                            // Display Banner Dot Navigation (corresponds with the number of post in $query)

                            for ($k = 0 ; $k < $count; $k++){

                                echo '<section class="owl-dot" id="'.$k.'"><span></span></section>';

                            }

                            ?>

                        </section>
                        <!--/.fueltech-dots-->

                    </div>

                </div>
                <!--/.column-->

                <div class="column">

                    <?php

                    if ($video) {


                        ?>

                        <section class="responsive-video-wrapper">

                    <span class="line hide-on-mobile"
                          style="background: #fff; width: 2px; position: absolute; left: 0; top: 0; height: 100%;"></span>

                            <video loop muted preload="none" poster="<?php echo $video_poster; ?>"
                                   class="hide-on-mobile">
                                <source src="<?php echo $video; ?>" type="video/mp4"/>
                            </video>

                            <img src="<?php echo $video_poster; ?>" alt="" class="show-on-mobile ui fluid image">

                        </section>
                        <!--/.responsive-video-wrapper-->

                        <?php

                    } else {

                        ?>

                        <img src="<?php echo $image[0]; ?>" alt="" class="float-right banner-image">

                        <?php

                    }

                    ?>

                </div>
                <!--/.column-->

            </section>
            <!--/#banner-->

            <?php

        endwhile;

    endif;

    ?>

</section>
<!--/#rotating-banner-->
